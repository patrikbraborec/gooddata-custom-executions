# GoodData Custom Execution

## Svelte with GoodData JS SDK

The folder `svelte-with-gooddata-js-sdk` contains an example of how to execute GoodData Insight within the Svelte framework.

If you want to run it, execute `yarn dev` and visit the page `http://localhost:8080`.

## Next.js with GoodData API

The folder `next-js-with-gooddata-api` contains an example of how to use GoodData API within next.js framework.

If you want to run it, execute `yarn dev` and visit the page `http://localhost:3000`.