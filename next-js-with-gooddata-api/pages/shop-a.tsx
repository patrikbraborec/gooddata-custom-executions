import type { GetServerSideProps, NextPage } from "next";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import { fetchAfm } from "../utils/fetch-afm";
import { fetchAfmResult } from "../utils/fetch-afm-result";
import { FunnelChart } from "../components/funnel-chart";
import { afmDataToFunnelData, FunnelData } from "../utils/afm-data-to-funnel-data";

interface ShopAProps {
    error: boolean;
    data: FunnelData[];
}

/**
 * The component is almost same with shop-b. It would be better to put common things into a single component.
 */
const ShopA: NextPage<ShopAProps> = ({ error, data }) => {
    if (error) {
        return (
            <div className={styles.container}>
                <Head>
                    <title>Dashboard - shop A</title>
                    <meta name="description" content="Dashboard"/>
                    <link rel="icon" href="/favicon.ico"/>
                </Head>

                <div>
                    Apparently, there is TOO MANY DATA POINTS TO DISPLAY 🔥
                </div>
            </div>
        );
    }
    return (
        <div className={styles.container}>
            <Head>
                <title>Dashboard</title>
                <meta name="description" content="Dashboard"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <div>
                Hello Shop A 🖖. Let's display some visualizations! 🚀

                <div className={styles.chart}>
                    <FunnelChart data={data}/>
                </div>
            </div>
        </div>
    );
};

export const getServerSideProps: GetServerSideProps<ShopAProps> = async () => {
    const url = process.env.GOODDATA_URL || "";
    const token = process.env.GOODDATA_BEARER_TOKEN || "";
    const workspace = "shop-a";
    let props: ShopAProps = {
        error: false,
        data: [],
    };

    try {
        const afmData = await fetchAfm(url, token, workspace);
        const afmResultData = await fetchAfmResult(url, token, workspace, afmData.executionResponse.links.executionResult);

        props = {
            ...props,
            data: afmDataToFunnelData(afmResultData),
        };
    } catch (err) {
        console.error(err);

        props = {
            ...props,
            error: true,
        };
    }

    return {
        props,
    };
};

export default ShopA;
