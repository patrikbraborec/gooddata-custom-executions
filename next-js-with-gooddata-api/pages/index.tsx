import type { GetServerSideProps, NextPage } from "next";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import { fetchProfile } from "../utils/fetch-profile";
import { fetchAfm } from "../utils/fetch-afm";
import { fetchAfmResult } from "../utils/fetch-afm-result";
import { FunnelChart } from "../components/funnel-chart";
import { afmDataToFunnelData, FunnelData } from "../utils/afm-data-to-funnel-data";

interface HomeProps {
    organizationName: string;
    error: boolean;
    data: FunnelData[];
}

const Home: NextPage<HomeProps> = ({ organizationName, error, data }) => {
    if (error) {
        return (
            <div className={styles.container}>
                <Head>
                    <title>Dashboard</title>
                    <meta name="description" content="Dashboard"/>
                    <link rel="icon" href="/favicon.ico"/>
                </Head>

                <div>
                    Apparently, there is TOO MANY DATA POINTS TO DISPLAY 🔥
                </div>
            </div>
        );
    }
    return (
        <div className={styles.container}>
            <Head>
                <title>Dashboard</title>
                <meta name="description" content="Dashboard"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <div>
                Hello {organizationName} 🖖. Let's display some visualizations! 🚀

                <div className={styles.chart}>
                    <FunnelChart data={data}/>
                </div>
            </div>
        </div>
    );
};

export const getServerSideProps: GetServerSideProps<HomeProps> = async () => {
    const url = process.env.GOODDATA_URL || "";
    const token = process.env.GOODDATA_BEARER_TOKEN || "";
    const workspace = process.env.GOODDATA_PARENT_WORKSPACE || "";
    let props: HomeProps = {
        organizationName: "",
        error: false,
        data: [],
    };

    try {
        const profileData = await fetchProfile(url, token);
        const afmData = await fetchAfm(url, token, workspace);
        const afmResultData = await fetchAfmResult(url, token, workspace, afmData.executionResponse.links.executionResult);

        props = {
            ...props,
            organizationName: profileData.organizationName,
            data: afmDataToFunnelData(afmResultData),
        };
    } catch (err) {
        console.error(err);

        props = {
            ...props,
            error: true,
        };
    }

    return {
        props,
    };
};

export default Home;
