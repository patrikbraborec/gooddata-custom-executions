/**
 * Just for demo purposes - the request is copy & paste from network console.
 */
export const fetchProfile = async (url: string, token: string) => {
    let res;

    try {
        res = await fetch(`${url}/api/v1/profile`, {
            "headers": {
                "Authorization": `Bearer ${token}`,
            },
        });
    } catch (err) {
        console.error(err);
        throw err;
    }

    return res.json();
};