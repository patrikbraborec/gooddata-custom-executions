/**
 * Just for demo purposes - the request is copy & paste from network console.
 */
export const fetchAfm = async (url: string, token: string, workspace: string) => {
    let res;

    try {
        res = await fetch(`${url}/api/v1/actions/workspaces/${workspace}/execution/afm/execute`, {
            "headers": {
                "Authorization": `Bearer ${token}`,
                "content-type": "application/json",
            },
            "method": "POST",
            "body": "{\"resultSpec\":{\"dimensions\":[{\"localIdentifier\":\"dim_0\",\"itemIdentifiers\":[\"a_c72336fe51a845ef961b720a6ec5e39d_product_name\"],\"sorting\":[{\"attribute\":{\"attributeIdentifier\":\"a_c72336fe51a845ef961b720a6ec5e39d_product_name\",\"sortType\":\"DEFAULT\"}}]},{\"localIdentifier\":\"dim_1\",\"itemIdentifiers\":[\"measureGroup\"]}],\"totals\":[]},\"execution\":{\"measures\":[{\"localIdentifier\":\"m_c72336fe51a845ef961b720a6ec5e39d_revenue\",\"definition\":{\"measure\":{\"item\":{\"identifier\":{\"id\":\"c72336fe51a845ef961b720a6ec5e39d:revenue\",\"type\":\"metric\"}}}}}],\"attributes\":[{\"label\":{\"identifier\":{\"id\":\"c72336fe51a845ef961b720a6ec5e39d:product_name\",\"type\":\"label\"}},\"localIdentifier\":\"a_c72336fe51a845ef961b720a6ec5e39d_product_name\"}],\"filters\":[],\"auxMeasures\":[]},\"settings\":{}}",
        });
    } catch (err) {
        console.error(err);
        throw err;
    }

    return res.json();
};
