export type FunnelData = {
    id: string,
    value: number,
    label: string
}

/**
 * It would be great to type AFM Result.
 * Very naive way how to transform data - just for demo purposes!
 */
export const afmDataToFunnelData = (afmData: any): FunnelData[] => {
    if (!afmData || !afmData.data || !afmData.dimensionHeaders) {
        return [];
    }

    const data = afmData.data;
    const headers = afmData.dimensionHeaders[0].headerGroups[0].headers;

    if (data.length !== headers.length) {
        return [];
    }

    const funnelDataWithoutSorting = data.map((item: any, index: number) => ({
        value: item[0],
        label: headers[index].attributeHeader.primaryLabelValue,
    }));
    const funnelDataWithSorting = funnelDataWithoutSorting.sort((a: any, b: any) => b.value - a.value);

    return funnelDataWithSorting.map((item: any, index: number) => ({
        id: index.toString(),
        value: item.value,
        label: item.label,
    }));
};