/**
 * Just for demo purposes - the request is copy & paste from network console.
 */
export const fetchAfmResult = async (url: string, token: string, workspace: string, executionId: string) => {
    let res;

    try {
        res = await fetch(`${url}/api/v1/actions/workspaces/${workspace}/execution/afm/execute/result/${executionId}`, {
            "headers": {
                "Authorization": `Bearer ${token}`,
            },
            "method": "GET",
        });
    } catch (err) {
        console.error(err);
        throw err;
    }

    return res.json();
};
