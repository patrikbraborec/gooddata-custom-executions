# Nextjs with GoodData API

The example repository with Next.js and GoodData API. 

## Result

The root route contains a dashboard with one chart that displays all data:

![Chart all](./docs/chart-all.png)

The `/shop-a` route contains a dashboard with one chart where are applied data filters from GoodData:

![Chart shop A](./docs/chart-shop-a.png)

The `/shop-b` route contains a dashboard with one chart where are applied data filters (different from `/shop-a` route) from GoodData:

![Chart shop B](./docs/chart-shop-b.png)

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
